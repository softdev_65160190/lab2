/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

public class Lab2 {

    static char[][] table = {{'7', '8', '9'}, {'4', '5', '6'}, {'1', '2', '3'}};
    static char currentPlayer = 'X';
    static char number;
    static char YesNo = 'y';

    static void printWelcome() {
        System.out.println("WelcomeOX");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + table[i][j]);
            }
            System.out.println();
        }
    }

    static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

    static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    static void inputNumber() {
        Scanner kb = new Scanner(System.in);
        System.out.print("please input number: ");
        number = kb.next().charAt(0);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (number == table[i][j]) {
                    table[i][j] = currentPlayer;
                }
            }
        }
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer && table[i][1] == currentPlayer & table[i][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == currentPlayer && table[1][i] == currentPlayer && table[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    static boolean checkDiag1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDiag2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][table.length - 1 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkDiag1()) {
            return true;
        } else if (checkDiag2()) {
            return true;
        }
        return false;
    }

    static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;
    }

    static void printWin() {
        System.out.println(currentPlayer + " is Win");
    }

    static void printDraw() {
        System.out.println("Draw");
    }

    static boolean askCon() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to play again? (y/n): ");
        return sc.next().equalsIgnoreCase("y");
    }

    static void reGame() {
        table = new char[][]{{'7', '8', '9'}, {'4', '5', '6'}, {'1', '2', '3'}};
        currentPlayer = 'X';
    }

    public static void main(String[] args) {
        printWelcome();
        do {
            printTable();
            printTurn();
            inputNumber();
            if (isWin()) {
                printTable();
                printWin();
                break;
            }
            if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        } while (true);

        if (askCon()) {
            reGame();
            main(null);
        } else {
            System.out.println("Thank you for playing Tic-Tac-Toe!");
        }
    }
}
